import 'package:flutter/material.dart';

class AppTheme {
  static const TextStyle fase1 = TextStyle(
    fontFamily: 'WorkSans',
    color: Color(0xFFcc6600),
    fontSize: 18,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle item = TextStyle(
    fontFamily: 'WorkSans',
    color: Color(0xFF000000),
    fontSize: 18,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle pedra = TextStyle(
    fontFamily: 'WorkSans',
    color: Color(0xFFff3333),
    fontSize: 20,
    fontWeight: FontWeight.w600,
    letterSpacing: 1.2,
  );

  static const TextStyle foguinho = TextStyle(
    fontFamily: 'WorkSans',
    color: Color(0xFFff9900),
    fontSize: 20,
    fontWeight: FontWeight.w600,
    letterSpacing: 1.2,
  );

  static const TextStyle coroa = TextStyle(
    fontFamily: 'WorkSans',
    color: Color(0xFFffcc00),
    fontSize: 20,
    fontWeight: FontWeight.w600,
    letterSpacing: 1.2,
  );

  static const TextStyle display1 = TextStyle(
    fontFamily: 'WorkSans',
    color: Colors.black,
    fontSize: 30,
    fontWeight: FontWeight.w600,
    letterSpacing: 1.2,
  );

  static const TextStyle display2 = TextStyle(
    fontFamily: 'WorkSans',
    color: Colors.black,
    fontSize: 24,
    fontWeight: FontWeight.normal,
    letterSpacing: 1.1,
  );

  static final TextStyle heading = TextStyle(
    fontFamily: 'WorkSans',
    fontWeight: FontWeight.w800,
    fontSize: 22,
    color: Colors.grey[350],
    letterSpacing: 1.2,
  );

  static final TextStyle subHeading = TextStyle(
    fontFamily: 'WorkSans',
    fontWeight: FontWeight.w300,
    fontSize: 16,
    color: Colors.black,
  );
}
